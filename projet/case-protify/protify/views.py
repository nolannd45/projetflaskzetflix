from .app import app
from flask import render_template
from .models import get_sample
import yaml
from flask_wtf import FlaskForm
from wtforms import StringField , HiddenField
from wtforms.validators import DataRequired
from flask import url_for, redirect
from .app import db
from .models import Author 
from .models import get_author
from wtforms import PasswordField
from .models import User
from .models import Music
from hashlib import sha256
from flask_login import login_user , current_user, logout_user, login_required
from flask import request,flash
from sqlalchemy.sql import func

data = yaml.safe_load(open("extrait.yml"))

@app.route("/")
def home():
    return render_template(
        "home.html",
        title="Protify",
        musics=get_sample()
        )

@app.route("/apropos")
def aprop():
    return render_template("apropos.html")

@app.route("/music")
def music():
    return render_template(
        "music.html",
        title = "Liste de musique",
        musics = data
    )


class AuthorForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('Nom', validators=[DataRequired()])

    @app.route("/edit/author/<int:id>")
    @login_required
    def edit_author(id):
        a = get_author(id)
        f = AuthorForm(id=a.id, name=a.name)
        return render_template("edit-author.html",author=a, form=f)

    @app.route("/edit/new_author")
    def edit_new_author():
        a = None 
        f = AuthorForm()
        if f.validate_on_submit():
            if(notOnDB(f.name.data)):
                db.session.add(Author(name=f.name.data))
                db.session.commit()
        return render_template("edit-new-author.html", form=f)
 

    @app.route("/save/author/", methods=("POST",))
    def save_author():
        a = None
        f = AuthorForm()
        if f.validate_on_submit():
            id = int(f.id.data)
            a = get_author(id)
            a.name = f.name.data
            db.session.commit()
            return redirect(url_for('home'))
        a = get_author(int(f.id.data))
        return render_template("edit-author.html",author=a, form=f)


class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None

    @app.route("/inscription/", methods=("GET","POST",))
    def sinscrire():
        a = None
        f = LoginForm()
        test = bool(User.query.filter_by(username=f.username.data).first())
        if not f.is_submitted():
            f.next.data = request.args.get("next")
        elif f.validate_on_submit():
            if not (test):
                m = sha256()
                m.update(f.password.data.encode())
                u = User(username=f.username.data, password=m.hexdigest())
                db.session.add(u)
                db.session.commit()
                return redirect(url_for("home"))
        
        return render_template("inscription.html",form=f)

    @app.route("/login/", methods=("GET","POST",))
    def login():
        f = LoginForm()
        if not f.is_submitted():
            f.next.data = request.args.get("next")
        elif f.validate_on_submit():
            user = f.get_authenticated_user()
            if user:
                login_user(user)
                next = f.next.data or url_for("home")

                return redirect(url_for("home"))
        return render_template("login.html",form=f)

   

    @app.route ("/logout/")
    def logout ():
        logout_user()
        return redirect ( url_for ('home'))

    @app.route('/inscription', methods=['GET'])
    def inscription():
        return render_template('inscription.html')


class AddForm(FlaskForm):
    title = StringField('title')
    img = StringField('img')
    parent = StringField('parent')
    genre = StringField('genre')
    next = HiddenField()


    @app.route("/ajouter", methods=("GET","POST",))
    def ajouter():
        a = None
        f = AddForm()
        test = bool(Music.query.filter_by(title=f.title.data).first())
        if not f.is_submitted():
            f.next.data = request.args.get("next")
        elif f.validate_on_submit():
            if not (test):
                max_id= db.session.query(func.max(Music.id)).scalar()
                max_aid= db.session.query(func.max(Music.author_id)).scalar()
                aut_id = db.session.query()
                u = Music(id=max_id+1, title=f.title.data, img=f.img.data, parent=f.parent.data, genre= f.genre.data,author_id=max_aid)
                
                db.session.add(u)
                
                db.session.commit()
                return redirect(url_for("home"))
        
        return render_template("ajouter.html",form=f)